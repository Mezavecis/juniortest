$(document).ready(function () {
    // Concatenates dimensions, separates with 'x', sends value:
    function dimensions() {
        var dimensions = [];
        dimensions[0] = $("input#Height").val();
        dimensions[1] = $("input#Width").val();
        dimensions[2] = $("input#Length").val();
        var output = dimensions.join([separator = 'x'])
        document.getElementById("hidden").value = output;
    }
    // Changes form/input depending on type:
    function display() {
        var type = $("#select").val();
        switch (type) {
            case "DVD":
                $("div#type").html('Size (in MB)' + '<input type="number" name="Value">');
                break;
            case "Book":
                $("div#type").html('Weight (in KG)' + '<input type="number" name="Value">');
                break;
            case "Furniture":
                $("div#type").html(
                    'Height (in CM) ' + '<input type="number" id="Height">' +
                    'Width (in CM) ' + '<input type="number" id="Width">' +
                    'Length (in CM) ' + '<input type="number" id="Length">' +
                    // Hidden input for concatenated dimensions:
                    '<input type="hidden" name="Value" id="hidden">'
                );
                dimensions();
                break;
            default:
                alert("ERROR!");
        }
    }
    // Send form to handler:
    $("#save").click(function () {
        var SKU = $("input[name=SKU]").val();
        var Name = $("input[name=Name]").val();
        var Price = $("input[name=Price]").val();
        var Type = $("#select").val();
        var Value = $("input[name=Value]").val();
        $.post("includes/handler.inc.php", {
            SKU: SKU,
            Name: Name,
            Price: Price,
            Type: Type,
            Value: Value
        }, function (data, status) {
            alert("Data: " + data,
                "Status: " + status);
        });
    });
    display();
    $("select").change(display);
    $("div#type").change(dimensions);
});