$(document).ready(function () {
    // Check all checkboxes:
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
    // Delete function:
    $("#delete").click(function () {
        var array = [];
        var arrayJoin;
        // Select checked checkboxes:
        $.each($("input[type='checkbox']:checked"), function () {
            // Store values as array:
            // ! Value = SKU
            array.push($(this).val());
            // Convert to string:
            arrayJoin = array.join("', '");
            // Add symbols for query:
            check = "'" + arrayJoin + "'";
        })
        // Send to handler:
        $.post("includes/handler.inc.php", {
            check: check,
        },
            function (data, status) {
                alert("Data: " + data,
                    "Status: " + status);
            });
        location.reload();
    })
});