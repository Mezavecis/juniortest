<?php
include './includes/autoLoader.inc.php';
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./styles/new.css">
    <title>Product add:</title>
</head>

<body>
    <div class="container">
        <nav class="d-flex flex-row justify-content-between">
            <h1>PRODUCT ADD:</h1>
            <aside>
                <a href="./index.php">
                    <button class="btn btn-link">
                        LIST
                    </button>
                </a>
                <button class="btn btn-success" id="save">
                    SAVE
                </button>
            </aside>
        </nav>
        <hr>
        <div id="form">
            SKU
            <input class="row" type="text" name="SKU" id="sku">
            Name
            <input class="row" type="text" name="Name" id="name">
            Price
            <input class="row" type="number" name="Price" step=".01" id="price">
            Type
            <select class="row" name="Type" id="select">
                <option value="DVD" id="dvd">DVD</option>
                <option value="Book" id="book">Book</option>
                <option value="Furniture" id="furniture">Furniture</option>
            </select>
        </div>
        <hr>
        <div id="type">
            <!-- Dynamic form: -->
        </div>
    </div>
    <!-- Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Individual -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="script\new.js"></script>
</body>

</html>