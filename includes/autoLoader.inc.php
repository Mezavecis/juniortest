<?php
// Class autoload:

spl_autoload_register('autoLoad');

function autoLoad($class)
{
    // Get current path location:
    $url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    // Check location and change autoload path accordingly:
    if (strpos($url, 'includes') !== false) {
        $path = '../classes/';
    } else if (strpos($url, 'classes') !== false) {
        $path = './';
    } else $path = "./classes/";

    // Add extension and declare full path:
    $ext = ".class.php";
    $fullPath = $path . $class . $ext;

    // Check file:
    if (!file_exists($fullPath)) {
        echo "File not found!";
        return false;
    }

    // Require class:
    require_once $fullPath;
}
