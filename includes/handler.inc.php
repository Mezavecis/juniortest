<?php
require_once './autoLoader.inc.php';
// Autoloader not loading class '$_POST['Type']' on line 9
require '../classes/types.class.php';

// Check if '../new.php' sends data..
if (isset($_POST['SKU'], $_POST['Name'], $_POST['Price'], $_POST['Type'], $_POST['Value'])) {

    // create new object Type product
    $product = new $_POST['Type']($_POST['SKU'], $_POST['Name'], $_POST['Price'], $_POST['Value']);

    // send product to SQL class and databse
    $post = new SQL();
    $post->postAll($product);
}

// Check if '../index.php' sends data..
if (isset($_POST['check'])) {
    // create new Sql object,
    $delete = new Sql();
    // send data to delete method:
    $delete->deleteAll($_POST['check']);
}
