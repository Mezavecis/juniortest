<?php
class DVD extends Product
{
    public $Type = self::class;
    public $symbol = " MB";

    public function __construct($SKU, $Name, $Price, $Value)
    {
        $this->SKU = $SKU;
        $this->Name = $Name;
        $this->Price = $Price;
        $this->Value = $Value;
    }
}

class Book extends Product
{
    public $Type = self::class;
    public $symbol = " KG";

    public function __construct($SKU, $Name, $Price, $Value)
    {
        $this->SKU = $SKU;
        $this->Name = $Name;
        $this->Price = $Price;
        $this->Value = $Value;
    }
}

class Furniture extends Product
{
    public $Type = self::class;
    public $symbol = " CM";

    public function __construct($SKU, $Name, $Price, $Value)
    {
        $this->SKU = $SKU;
        $this->Name = $Name;
        $this->Price = $Price;
        $this->Value = $Value;
    }
}
