<?php
class DB
{
    // Properties:
    private $host;
    private $user;
    private $pass;
    private $db;

    // Connect to database:
    private function connect()
    {
        // Server settings:
        $this->host = 'localhost';
        $this->user = 'root';
        $this->pass = 'test';
        $this->db = 'products';

        // Make connection:
        $conn = new mysqli($this->host, $this->user, $this->pass, $this->db);

        // Check connection:
        if ($conn->connect_error) {
            die("Connection unsuccessful! \n" . $conn->connect_errno . $conn->connect_error);
        }

        // Return connection:
        return $conn;
    }

    // Send query:
    protected function send($sql)
    {
        $result = $this->connect()->query($sql);
        return $result;
    }

    // Check statuss:
    protected function statuss($result, $sql)
    {
        if (!$result) {
            echo "SQL: " . $sql;
            echo mysql_errno(connect()) . ": " . mysql_error(connect());
        } else {
            echo "Success!";
        }
    }
}
