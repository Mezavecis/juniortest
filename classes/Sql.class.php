<?php
class Sql extends DB
{
    const BR = '<br/>';

    // Get all data from database:
    protected function getAll()
    {
        // Declare GET query:
        $sql = "SELECT * FROM products";

        // Send query:
        $result = $this->send($sql);

        // Return data as an array:
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        } else return null;
    }

    // Post all data to database:
    public function postAll($product)
    {
        // Declare POST query
        $sql = "INSERT INTO products (SKU, Name, Price, Type, Value) values (
        '$product->SKU',
        '$product->Name',
        '$product->Price',
        '$product->Type',
        '$product->Value')";

        // Send query:
        $result = $this->send($sql);

        // Return statuss:
        $this->statuss($result, $sql);
    }

    // Delete all data from database:
    public function deleteAll($check)
    {
        // Declare query delete row:
        $sql = "DELETE FROM products WHERE SKU in ($check)";

        // Send query:
        $result = $this->send($sql);

        // Return statuss:
        $this->statuss($result, $sql);
    }

    // Data print method:
    public function showAll()
    {
        // Get data from database:
        $result = $this->getAll();

        // Print data:
        if ($result > 0) {
            foreach ($result as $row) {
                echo '<span id="list">';
                echo "SKU: " . $row["SKU"] .  self::BR;
                echo "Name: " .  $row["Name"] . self::BR;
                echo "Price: " .  $row["Price"] . " $" . self::BR;
                echo "Type: " .  $row["Type"] . self::BR;
                // Add symbol to type:
                switch ($row["Type"]) {
                    case 'DVD':
                        $symbol = " MB";
                        break;
                    case 'Book':
                        $symbol = " KG";
                        break;
                    case 'Furniture':
                        $symbol = " CM";
                        break;
                    default:
                        echo "ERROR!";
                        break;
                }
                echo "Value: " .  $row["Value"] . $symbol . self::BR;
                // Checkbox for delete action:
                // ! Value = SKU
                echo '<input type="checkbox" value="' . $row["SKU"] . '" id="check">';
                echo "</span>";
            }
        } else echo "0 results!";
    }
}
