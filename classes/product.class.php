<?php

abstract class Product
{
    const BR = "<br>";

    public $SKU;
    public $Name;
    public $Price;
    public $Value;

    public function print()
    {
        echo '<span id="list">';
        echo 'SKU: ' . $this->SKU . self::BR;
        echo 'Name: ' . $this->Name . self::BR;
        echo 'Price: ' . $this->Price . self::BR;
        echo 'Type: ' . $this->Type . self::BR;
        echo 'Value: ' . $this->Value . $this->symbol . self::BR;
        echo "</span>";
    }
}
